import boto3
from os import environ
import logging

logging.basicConfig(format='%(asctime)s:%(levelname)s: %(message)s', level=logging.INFO)

s3 = boto3.client('s3',
                    endpoint_url=environ.get('URL'),
                    aws_access_key_id=environ.get('ACCESSKEY'),
                    aws_secret_access_key=environ.get('SECRETKEY'))

logging.info("Trying to access S3 at endpoint : " + environ.get('URL') + " , with accessKey : " + environ.get('ACCESSKEY'))

response = s3.list_buckets()
logging.info('Available buckets list :')
for item in response['Buckets']:
    print(item['CreationDate'], item['Name'])
