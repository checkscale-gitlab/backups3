import boto3
import logging
import tarfile
import os
import shutil
import argparse


def make_tarfile(source_dir, output_filename):
    with tarfile.open(output_filename, "w:gz") as tar:
        tar.add(source_dir, arcname=os.path.basename(source_dir))


parser = argparse.ArgumentParser(description='''A script to do backups on S3 systems (using same API as Amazon S3) with boto3, 
                                                you can backup a file or a directory if you compress it (you can also compress a single file).
                                                This script is shipped in a container image to be used on Kubernetes. It needs 3 environment
                                                variables to run : URL, ACCESSKEY and SECRETKEY (the info to access S3)''')
parser.add_argument("bucket", type=str, help="Name of the bucket where you want to upload your backup")
parser.add_argument("dataPath", type=str, help="Where the file or directory to backup is located (put absolute path with the file/directory name)")
parser.add_argument("fileName", type=str, help="The name of your backup file once on S3")
parser.add_argument("-c", "--compress", action="store_true", help="Backup the file making it a compressed archive (don't forget the .tgz at the end of the file name)")
parser.add_argument("-q", "--quiet", action="store_true", help="Disable logging")
parser.add_argument('--verbose', '-v', action='count', default=0, help="There is 2 levels of verbosity, juste use the flag once or twice")
args = parser.parse_args()


if args.quiet:
    logging.disabled = True
elif args.verbose>=2:
    logging.basicConfig(format='%(asctime)s:%(levelname)s: %(message)s', level=logging.DEBUG)
elif args.verbose==1:
    logging.basicConfig(format='%(asctime)s:%(levelname)s: %(message)s', level=logging.INFO)
else:
    logging.basicConfig(format='%(asctime)s:%(levelname)s: %(message)s', level=logging.WARNING)


bucket=args.bucket
logging.debug("The value of Bucket is : " + bucket)
dataPath=args.dataPath
dataName=os.path.basename(dataPath)
logging.debug("The value of dataPath is : " + dataPath)
fileName=args.fileName
logging.debug("The value of fileName is : " + fileName)


if (os.path.isfile(dataPath)):
    logging.info("copying file " + dataPath +  " in /tmp/")
    shutil.copy2(dataPath,"/tmp/")
    logging.info("copy completed")
    
    if args.compress:
        logging.info("compressing file " + dataName)
        make_tarfile("/tmp/" + dataName,"/tmp/" + fileName)

elif (os.path.isdir(dataPath)):
    logging.info("copying directory " + dataPath +  " in /tmp/")
    shutil.copytree(dataPath, "/tmp/data")

    if args.compress:
        logging.info("copy completed, creating archive : " + fileName)
        make_tarfile("/tmp/data","/tmp/" + fileName)
    else:
        logging.error("You are trying to backup a directory without compressing it, this is not suppported")
        exit(1)
else:
    logging.error(dataPath + " is not a directory or a file, or does not exist")
    exit(1)

s3 = boto3.resource('s3',
                    endpoint_url=os.environ.get('URL'),
                    aws_access_key_id=os.environ.get('ACCESSKEY'),
                    aws_secret_access_key=os.environ.get('SECRETKEY'))
logging.debug("You are trying to access S3 endpoint : " + os.environ.get('URL') + " , with accessKey : " + os.environ.get('ACCESSKEY'))

if args.compress:
    logging.info("uploading " + fileName + " in bucket " + bucket)
    s3.Bucket(bucket).upload_file("/tmp/" + fileName, fileName)
else:
    logging.info("uploading " + dataName + " in bucket " + bucket + " as " + fileName)
    s3.Bucket(bucket).upload_file("/tmp/" + dataName, fileName)
logging.info("upload completed")
exit(0)
